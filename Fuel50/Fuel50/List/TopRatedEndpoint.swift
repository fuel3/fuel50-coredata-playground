//
//  TopRatedEndpoint.swift
//  Fuel50
//
//  Created by ldrevych on 10.11.2023.
//

import Foundation
import Foundation

enum GetTopRatedEnpoint: Endpoint {

    case getTopRated
    
    var method: HTTPMethod {
        switch self {
            case .getTopRated: return .GET
        }
    }
    
    var path: String {
        switch self {
            case .getTopRated: return "top_rated"
        }
    }
    
    var headers: [String : String] {
        .getDefaultHeaders()
    }
    
    var body: Data? {
        nil
    }
}
