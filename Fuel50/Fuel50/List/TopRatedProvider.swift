//
//  TopRatedProvider.swift
//  Fuel50
//
//  Created by ldrevych on 10.11.2023.
//

import Foundation

protocol TopRatedMovieProviderProtocol: AnyObject {
    func getTopRatadMovie(with endpoint: Endpoint, parameters: [String : String]) async throws -> [MovieResult]
}

final class TopRatedMovieProvider: APIClient, TopRatedMovieProviderProtocol {
    
    private let dao: CoreDataDao
    
    init(dao: CoreDataDao = CoreDataDao()) {
        self.dao = dao
    }
    
    public func getTopRatadMovie(with endpoint: Endpoint, parameters: [String : String]) async throws ->  [MovieResult] {
        if pathMonitor.currentPath.status == .satisfied || pathMonitor.currentPath.status == .unsatisfied {
            let results: MovieList = try await sendRequest(with: endpoint, parameters: parameters)
            try await saveImagas(results: results.result)
            return results.result
        } else {
            return try await getTopRatedDBMovies()
        }
    }
    
    private func getTopRatedDBMovies() async throws -> [MovieResult] {
        var movieResults: [MovieResult] = []
        do {
            let fetchedEntities: [DBMovieItem]? = try await dao.fetch(with: nil, sort: nil, fetchLimit: 20)
            guard let model = fetchedEntities else { return [] }
            
            for result in model {
                if let poster = result.poster, let backdrop = result.backdrop {
                    movieResults.append(MovieResult(poster: String(data: poster, encoding: .utf8), adult: result.adult, overview: result.overview ?? "", realeaseDate: result.realeaseDate ?? "", id: Int(result.id) , originalTitle: result.originalTitle ?? "", originalLanguage: result.originalLanguage ?? "", title: result.title ?? "", backdrop: String(data: backdrop, encoding: .utf8) ?? ""))
                }
            }
            
        } catch {
            print("Error fetching entities: \(error.localizedDescription)")
        }
        
        return movieResults
    }
    
    private func saveImagas(results: [MovieResult]) async throws {
        for value in results {
            try await dao.create(entityName: DBMovieItem.name) { (movie: DBMovieItem) in
                movie.adult = value.adult
                movie.id = Int64(value.id)
                movie.originalLanguage = value.originalLanguage
                movie.originalTitle = value.originalTitle
                movie.realeaseDate = value.realeaseDate
                movie.title = value.title
                movie.backdrop = value.backdrop.data(using: .utf8)
                movie.poster = value.poster?.data(using: .utf8)
            }
        }
    }
}
