//
//  ContentView.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject var viewModel: MovieViewModel
    @State private var currentPage = 1
    
    var body: some View {
        NavigationView {
            ScrollableContent(movies: viewModel.movies)
        }.navigationTitle("Movies")
            .onAppear(perform: {
                viewModel.fetchTopRatedMovies(for: currentPage)
            })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let provider = TopRatedMovieProvider()
        ContentView(viewModel: MovieViewModel(provider: provider))
    }
}

struct ScrollableContent: View {
    
    var movies: [MovieResult]
    
    var body: some View {
        List(movies) { movie in
            MovieListItem(movie: movie)
        }.padding(.horizontal, 10)
    }
    
    private func generateImages(minValue: Int, maxValue: Int) -> [Image] {
        var result: [Image] = []
        let randomCount = Int.random(in: minValue...maxValue)
        for _ in 0..<randomCount {
            let image = Image("horse")
            result.append(image)
        }
        return result
    }
}

struct MovieListItem: View {
    
    let movie: MovieResult
    
    var body: some View {
        HStack(spacing: 16) {
            if let posterImage = UIImage(named: "horse") {
                Image(uiImage: posterImage)
                    .resizable()
                    .frame(width: 30, height: 30)
                    .cornerRadius(8)
            } else {
                Color.gray
                    .frame(width: 60, height: 90)
                    .cornerRadius(8)
            }
            VStack(alignment: .leading, spacing: 4) {
                Text(movie.originalTitle)
                    .font(.headline)
                    .lineLimit(2)
            }
        }
        .padding(.vertical, 8)
        .padding(.horizontal, 12)
    }
}
