//
//  MovieViewModel.swift
//  Fuel50
//
//  Created by ldrevych on 10.11.2023.
//

import Foundation
import SwiftUI

@MainActor
final class MovieViewModel: ObservableObject {
    
    //MARK: - Public
    @Published var movies: [MovieResult] = []
    
    //MARK: - Private
    private var provider: TopRatedMovieProviderProtocol
    
    init(provider: TopRatedMovieProviderProtocol) {
        self.provider = provider
    }
    
    func fetchTopRatedMovies(for page: Int) {
        Task {
            do {
                let endpoint = GetTopRatedEnpoint.getTopRated
                let result = try await provider.getTopRatadMovie(with: endpoint, parameters: ["api_key": Config.secret, "page": "\(page + 1)"])
                movies = result
            } catch {
                print("Error fetching movies: \(error)")
            }
        }
    }
}
