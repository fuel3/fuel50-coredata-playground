//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//

import SwiftUI

@main
struct Fuel50App: App {

    var body: some Scene {
        WindowGroup {
            let provider = TopRatedMovieProvider()
            ContentView(viewModel: MovieViewModel(provider: provider))
        }
    }
}
