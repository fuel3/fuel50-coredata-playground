//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//


import Foundation

public enum DBError: LocalizedError {
    //MARK: - DBError errors
    case invalidEntityName
    case fetchError(to: String)
    case cannotPerformError(info: String)
}
