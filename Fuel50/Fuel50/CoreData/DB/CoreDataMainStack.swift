//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//


import Foundation
import CoreData

final public class CoreDataMainStack {
    
    //MARK: - Context
    private lazy var saveContext: NSManagedObjectContext = {
        let context = persistentContainer.viewContext
        context.automaticallyMergesChangesFromParent = true
        return context
    }()
    
    private lazy var syncContext: NSManagedObjectContext = {
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.parent = persistentContainer.newBackgroundContext()
        return context
    }()
    
    //MARK: - Private
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Fuel50")
        container.loadPersistentStores { storeDescription, error in
            if let error = error {
                debugPrint("CoreDate Error: \(error), Due to: \(error.localizedDescription)")
            }
        }
        return container
    }()
    
    private var observers: [NSObjectProtocol] = []
    
    //MARK: - Life cycle
    init() {
        setup()
    }
    
    deinit {
        observers.removeAll()
    }
    
    //MARK: - Public func
    public func reset() {
        syncContext.reset()
        saveContext.reset()
    }
    
    public func performTask(_ block: @escaping (NSManagedObjectContext) throws -> Void) async throws {
        try await syncContext.perform {
            do {
                try block(self.syncContext)
            } catch {
                throw DBError.cannotPerformError(info: error.localizedDescription)
            }
        }
    }
    
    //MARK: - Private func
    private func setup() {
        let syncObserver = syncContext.didSave { [weak self] notification in
            self?.monitorChanges(object: notification)
            self?.saveContext.perform {
                self?.saveContext.performMerge(from: notification)
                try? self?.saveContext.save()
            }
        }
    
        observers.append(contentsOf: [syncObserver])
    }
    
    private func monitorChanges(object: MOCNotification) {
        if let inserts = object.objects(forKey: NSInsertedObjectsKey), inserts.count > 0 {
            print("--- INSERTS ---")
            print(inserts)
            print("---------------")
        }
        
        if let update = object.objects(forKey: NSUpdatedObjectsKey), update.count > 0 {
            print("--- UPDATES ---")
            print(update)
            print("---------------")
        }
        
        if let deletes = object.objects(forKey: NSDeletedObjectsKey), deletes.count > 0 {
            print("--- DELETES ---")
            print(deletes)
            print("---------------")
        }
    }
}
