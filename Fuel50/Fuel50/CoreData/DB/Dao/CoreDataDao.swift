//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//

import CoreData

final public class CoreDataDao {
    
    private let coreDataStack = CoreDataMainStack()
    
    //MARK: - Life cycle
    deinit {
        debugPrint("deinit -> ", self)
    }
    
    //MARK: - Public
    public func fetch<T: NSManagedObject>(with predicate: NSPredicate?,
                                          sort: [NSSortDescriptor]?, fetchLimit: Int) async throws -> [T]? {
        var result: [T] = []
        
        try await coreDataStack.performTask { context in
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: T.name)
            request.predicate = predicate
            request.fetchLimit = fetchLimit
            request.sortDescriptors = sort
            request.returnsObjectsAsFaults = false
            
            do {
                let objects = try context.fetch(request) as? [T]
                result = objects ?? []
            } catch {
                throw DBError.fetchError(to: error.localizedDescription)
            }
        }
        
        return result
    }
    
    public func delete<T: NSManagedObject>(object: T) async throws {
        try await coreDataStack.performTask { context in
            context.delete(object)
            try context.save()
        }
    }
    
    public func create<T: NSManagedObject>(entityName: String, _ configurate: @escaping (T) -> Void) async throws {
        try await coreDataStack.performTask { context in
            guard let entity = NSEntityDescription.entity(
                forEntityName: entityName,
                in: context
            ) else {
                throw DBError.invalidEntityName
            }
            
            let managedObj = T(entity: entity, insertInto: context)
            configurate(managedObj)
            try context.save()
        }
    }
}
