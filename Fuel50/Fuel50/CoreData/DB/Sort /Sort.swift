//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//


import Foundation

public enum Sort {
    case ASC
    case DESC
    
    var value: Bool {
        switch self {
            case .ASC:
                return true
            case .DESC:
                return false
        }
    }
}
