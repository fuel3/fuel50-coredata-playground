//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//


import CoreData

public final class MOCNotification {
        
    let notification: Notification
    
    init(notification: Notification) {
        guard notification.name == .NSManagedObjectContextDidSave else {
            debugPrint("Wrong notification")
            fatalError()
        }
        self.notification = notification
    }
    
    func objects(forKey key: String) -> Set<NSManagedObject>? {
        return (notification.userInfo?[key] as? Set)
    }
}
