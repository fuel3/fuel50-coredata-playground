//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//


import Foundation
import CoreData

//TODO: Check if we need this protocol
protocol MOCProtocol {
    associatedtype Entity
    func toEntity() -> Entity?
}

protocol ManagedObjectConvertible {
    associatedtype ManagedObject
    func toManagedObject(in context: NSManagedObjectContext) -> ManagedObject
}
