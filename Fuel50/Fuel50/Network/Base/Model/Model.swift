//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//

import Foundation

struct MovieList: Codable, Equatable {
    let page: Int
    let totalResults: Int?
    let totalPages: Int?
    let result: [MovieResult]
    
    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case result = "results"
    }
}

struct MovieResult: Codable, Identifiable, Hashable, Equatable {
    let poster: String?
    let adult: Bool
    let overview: String
    let realeaseDate: String
    let id: Int
    let originalTitle: String
    let originalLanguage: String
    let title: String
    let backdrop: String
    
    enum CodingKeys: String, CodingKey {
        case poster = "poster_path"
        case backdrop = "backdrop_path"
        case adult
        case overview
        case realeaseDate = "release_date"
        case id
        case originalTitle = "original_title"
        case originalLanguage = "original_language"
        case title
    }
}

extension MovieList {
    static func preview() -> Self {
        MovieList(page: 0, totalResults: 200, totalPages: 300, result: [
            MovieResult(poster: "/e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg", adult: false,
                        overview: "From DC Comics comes the Suicide Squad, an antihero team of incarcerated supervillains who act as deniable assets for the United States government, undertaking high-risk black ops missions in exchange for commuted prison sentences.",
                        realeaseDate: "2016-08-03", id: 2977623, originalTitle: "Suicide Squad",
                        originalLanguage: "en", title: "Suicide Squad", backdrop: "tmU7GeKVybMWFButWEGl2M4GeiP.jpg"),
            MovieResult(poster: "/e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg", adult: false,
                        overview: "From DC Comics comes the Suicide Squad, an antihero team of incarcerated supervillains who act as deniable assets for the United States government, undertaking high-risk black ops missions in exchange for commuted prison sentences.",
                        realeaseDate: "2016-08-03", id: 297764, originalTitle: "Suicide Squad",
                        originalLanguage: "en", title: "Suicide Squad", backdrop: "tmU7GeKVybMWFButWEGl2M4GeiP.jpg"),
            MovieResult(poster: "/e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg", adult: false,
                        overview: "From DC Comics comes the Suicide Squad, an antihero team of incarcerated supervillains who act as deniable assets for the United States government, undertaking high-risk black ops missions in exchange for commuted prison sentences.",
                        realeaseDate: "2016-08-03", id: 297751, originalTitle: "Suicide Squad",
                        originalLanguage: "en", title: "Suicide Squad", backdrop: "tmU7GeKVybMWFButWEGl2M4GeiP.jpg"),
            MovieResult(poster: "/e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg", adult: false,
                        overview: "From DC Comics comes the Suicide Squad, an antihero team of incarcerated supervillains who act as deniable assets for the United States government, undertaking high-risk black ops missions in exchange for commuted prison sentences.",
                        realeaseDate: "2016-08-03", id: 2, originalTitle: "Suicide Squad",
                        originalLanguage: "en", title: "Suicide Squad", backdrop: "tmU7GeKVybMWFButWEGl2M4GeiP.jpg"),
            MovieResult(poster: "/e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg", adult: false,
                        overview: "From DC Comics comes the Suicide Squad, an antihero team of incarcerated supervillains who act as deniable assets for the United States government, undertaking high-risk black ops missions in exchange for commuted prison sentences.",
                        realeaseDate: "2016-08-03", id: 29775343, originalTitle: "Suicide Squad",
                        originalLanguage: "en", title: "Suicide Squad", backdrop: "tmU7GeKVybMWFButWEGl2M4GeiP.jpg"),
            MovieResult(poster: "/e1mjopzAS2KNsvpbpahQ1a6SkSn.jpg", adult: false,
                        overview: "From DC Comics comes the Suicide Squad, an antihero team of incarcerated supervillains who act as deniable assets for the United States government, undertaking high-risk black ops missions in exchange for commuted prison sentences.",
                        realeaseDate: "2016-08-03", id: 421432, originalTitle: "Suicide Squad",
                        originalLanguage: "en", title: "Suicide Squad", backdrop: "tmU7GeKVybMWFButWEGl2M4GeiP.jpg")
        ])
    }
}
