//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//

import Foundation

final class Config {
    
    fileprivate enum Keys {
        static let secret = "SECRET"
    }
    
    static let secret: String = {
        guard let secret = Bundle.main.infoDictionary?[Keys.secret] as? String else {
            fatalError("NO SECRET")
        }
        return secret
    }()
    
}
