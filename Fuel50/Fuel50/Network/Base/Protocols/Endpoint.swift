//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//

import Foundation

protocol Endpoint {
    var method: HTTPMethod { get }
    var path: String { get }
    var headers: [String : String] { get }
    var body: Data? { get }
}
