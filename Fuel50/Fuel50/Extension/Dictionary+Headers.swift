//
//  Dictionary+Headers.swift
//  Fuel50
//
//  Created by ldrevych on 10.11.2023.
//

import Foundation

extension Dictionary where Key == String, Value == String {
    
   static func getDefaultHeaders() -> Self {
        let secret = Config.secret
        return [
            "Authorization": "Bearer \(secret)",
            "Content-Type": "application/json;charset=utf-8"
        ]
    }
}
