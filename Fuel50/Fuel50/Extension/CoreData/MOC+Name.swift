//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//

import CoreData

//MARK: - NSManagedObject + Name
extension NSManagedObject {
    static var name: String {
        return String(describing: self)
    }
}
