//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//

import CoreData

//MARK: - NSManagedObject + Perform Merge
extension NSManagedObjectContext {
    func performMerge(from: MOCNotification) {
        perform { [weak self] in
            self?.mergeChanges(fromContextDidSave: from.notification)
        }
    }
}
