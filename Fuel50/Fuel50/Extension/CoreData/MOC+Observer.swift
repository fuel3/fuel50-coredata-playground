//
//  Fuel50App.swift
//  Fuel50
//
//  Created by ldrevych on 14.10.2023.
//

import CoreData

//MARK: - NSManagedObject + Observer
public extension NSManagedObjectContext {
    
    func didSave(_ complition: @escaping (MOCNotification) -> Void) -> NSObjectProtocol {
        return NotificationCenter.default.addObserver(forName: .NSManagedObjectContextDidSave, object: self, queue: nil) {
            complition(MOCNotification(notification: $0))
        }
    }
    
    func didChange(_ complition: @escaping (MOCNotification) -> Void) -> NSObjectProtocol {
        return NotificationCenter.default.addObserver(forName: .NSManagedObjectContextObjectsDidChange, object: self, queue: nil) {
            complition(MOCNotification(notification: $0))
        }
    }
    
    func willSave(_ complition: @escaping (MOCNotification) -> Void) -> NSObjectProtocol {
        return NotificationCenter.default.addObserver(forName: .NSManagedObjectContextWillSave, object: self, queue: nil) {
            complition(MOCNotification(notification: $0))
        }
    }
}
